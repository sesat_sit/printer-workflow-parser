(ns printer-workflow-parser.core
  (:require [instaparse.core :as insta]
            [cheshire.core :as json]
            [clojure.string :refer [trim]]
            [clojure.java.io :as io])
  (:gen-class))


(def parser (insta/parser (-> "grammar.ebnf" io/resource slurp)))

(def tm {:key trim
         :value trim
         :kv hash-map
         :obj (fn [name & args] {name (vec args)})
         :elem vector})

(defn file-to-json [file]
  (let [in-file (io/as-file file)]
    (let [parsed (insta/parse parser (slurp in-file :encoding "utf-8") :total false)]
    (if (insta/failure? parsed)
      (throw (ex-info "Could not parse file" {:file in-file
                                              :error parsed}))
      (json/generate-string (insta/transform tm parsed))))))

(defn -main [in-file out-file]
  (let [parsed (insta/parse parser (slurp in-file) :total false)]
    (spit out-file (file-to-json in-file))))


(comment
  (spit "d:/foo" (json/generate-string (insta/transform tm (insta/parse parser (slurp "target/s.cfg") :total false))))
  )
